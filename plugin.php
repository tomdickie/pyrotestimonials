<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * FAQ Plugin
 *
 * Create lists of faqs
 * 
 * @author		Tom Dickie
 */
class Plugin_Testimonials extends Plugin
{
	public function Testimonials()
	{

        $testimonials = $this->db
        ->get('testimonials_testimonials')
        ->result_array();;

        return $testimonials;
	}
}
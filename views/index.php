<h2>{{ template:title }}</h2>
{{ if testimonials.total > 0 }}
<div id="testimonials">
    <h3>{{ helper:lang line="testimonials:title" }}</h3>
    {{ pagination:links }}
    <div id="questions">
        <ol>
            {{ testimonials.entries }}
            <li>{{ url:anchor segments="testimonials/#{{ id }}" title="{{ company }}" class="question" }}</li>
            {{ /testimonials.entries }}
        </ol>
    </div>
    <div id="answers">
        <h3>{{ helper:lang line="testimonials:body" }}</h3>
        <ol> 
            {{ testimonials.entries }}
            <li class="answer">
                <h4 id="{{ id }}">{{ title }}</h4>
                <p>{{ body }}</p>
            </li>
            {{ /testimonials.entries }}
        </ol>
    </div>
</div>
{{ else }}
<h4>{{ helper:lang line="testimonials:no_testimonials" }}</h4>
{{ endif }}